DIR='/home/wangq/Daniel_TWGBS/result/EpiPoly/'
filename=DIR+'EpiSubpopulation_result.csv'
d={}
f=open(filename)
for l in f:
	ls=l[:-1].split('\t')
	region=ls[0]
	sample=ls[1]
	pop=ls[2]
	if not d.has_key(region):
		d[region]={}
	if not d[region].has_key(sample):
		d[region][sample]={}
	if not d[region][sample].has_key(pop):
		d[region][sample][pop]=ls[3:6]


dcount={'LT':0,'ST':0,'MPP':0}
dmin={'LT':[],'ST':[],'MPP':[]}
for region,v in d.items():
	for sample,vv in v.items():
		if vv.has_key('111') and vv.has_key('222'):
			print region, sample,vv
			dcount[sample]+=1
			dmin[sample].append(min(float(vv['111'][1]),float(vv['222'][1])))
		

>>> dcount
{'LT': 18, 'MPP': 14, 'ST': 15}

>>> from numpy import *

>>> mean(dmin['LT'])
0.35818989429305559
>>> mean(dmin['ST'])
0.37156221812200002
>>> mean(dmin['MPP'])
0.37479623876728574

>>> median(dmin['MPP'])
0.38297872340400002
>>> median(dmin['ST'])
0.37209302325600002
>>> median(dmin['LT'])
0.37171052631599999

