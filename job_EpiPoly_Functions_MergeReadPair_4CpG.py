import pysam
import os
import bisect
import sys

pid=sys.argv[1] # sample ID
chrn=sys.argv[2] # chromosome number

######### paramters ################
posfile='/icgc/lsdf/mb/analysis/Reference/mm10/methylCtools/mm10_PhiX_Lambda_CGonly_Index.pos.gz' # the positions of CpG's in the genome
MIN_QUAL=20 # minimal base quality to consider meth or unmeth, bases lower than the cutoff is considered UNKOWN

SampleDIR='/home/wangq/Daniel_TWGBS/result/results_per_pid/'
SampleList=['LT','ST','MPP2','MPP']  # pids
#bamefile=SampleDIR+pid+'_temp/alignment/'+pid+'_merged.rmdup.bam'

inputDIR='/home/wangq/Daniel_TWGBS/result/EpiPoly/AllRegion/4CpG/'
outputDIR=inputDIR+'EpiScore_Meth/'

MINcov=30 # when picking the candidate regions, this criteria is already applied, here just to reinforce it, we can also make it higher than the 30 enforced by the candidate selection script

############## constant defined
METH=1
UNMETH=2
UNKOWN=0
SOFTCLIP_CIGAR=4

MIN_CPG=4 # minimal number of CpGs per read to be plotted

d_cg_nt={0:('C','T'),1:('G','A')} # define converted (unmeth) and unconverted bases for plus(0) and minus (1) strand

##### functions #################33
def find_gt(a, x):
  """
  Find leftmost value greater than x
  """
  i = bisect.bisect_right(a, x)
  if i != len(a):
    return i
  raise ValueError

def merge_d(d1,d2): 
  """
  merged dictionary d2 into dictionary d1 (both dictionaries have two levels of keys), and for
  """
  for k1 in d2.keys():
    if not d1.has_key(k1):
      d1[k1]={}
    for k2 in d2[k1].keys():
      if not d1[k1].has_key(k2):
        d1[k1][k2]=d2[k1][k2]
      else:
        d1[k1][k2]+=d2[k1][k2]


def sort_by_mean(ks):
  """
  sorted the keys in ks by the average methylation level, 
  e.g. the average for string '1210' is (1+2+1+1.5)/4
  return the sorted keys as a list
  """
  d={} # a dictionary d[1.5]=['2211','1212']
  for k in ks: # k is a string, e.g. '1220'
    n=0
    for kk in k:
      if kk==str(UNKOWN):
	n=n+1.5 # UNKOWN base is rated between METH(1) and UNMETH(2)
      else:
      	n=n+int(kk) # METH(1) and UNMETH(2)
    m=1.0*n/len(k) # the average methylation level of this string
    if not d.has_key(m):
      d[m]=[k] 
    else:
      d[m].append(k)
  l=[] # the string sorted by average meth
  ks=d.keys()
  ks.sort()
  for k in ks:
    for x in d[k]:
      l.append(x)
  return l


def Merge(s1,s2):
  """
  merge two strings of equal length
  if one of the string is UNKOWN at that position, take the one from the other string
  if there is conflict, use UNKOWN
  >>> Merge('1212','0211')
  '1210'
  """
  sUNKOWN=str(UNKOWN)
  L1=len(s1)
  if len(s2)!=L1:
    return -1
  s=''
  for i in range(L1):
    if s1[i]==s2[i]:
      s=s+s1[i]
    elif s1[i]==sUNKOWN:
      s=s+s2[i]
    elif s2[i]==sUNKOWN:
      s=s+s1[i]
    else:
      s=s+sUNKOWN
  return s

	
def MergeReadPair(v1,v2):
  """
  in paired-end sequencing, the paired-reads actually come from the same DNA molecue, 
  thus the information from the pair can be merged in epipolymorphims caclulation
  # two reads no overlap
  >>> MergeReadPair((0,'111'),(5,'222'))
  (0, '11100222')
  L1 contains L2
  >>> MergeReadPair((0,'1111111111111'),(5,'222'))
  (0, '1111100011111')
  # two reads overlap
  >>> MergeReadPair((0,'1111111'),(5,'22222222'))
  (0, '1111100222222')
  # swap L1 & L2
  >>> MergeReadPair((5,'222'),(0,'111'))
  (0, '11100222')
  >>> MergeReadPair((5,'222'),(0,'1111111111111'))
  (0, '1111100011111')
  >>> MergeReadPair((5,'22222222'),(0,'1111111'))
  (0, '1111100222222')
   """
  #unwrap the input object
  (CpG_local_index_start1,read_cpg_bin1)=v1
  (CpG_local_index_start2,read_cpg_bin2)=v2
  # make sure p1(b1) is the on the 5' end of p2(b2)
  if CpG_local_index_start1<=CpG_local_index_start2:
    p1=CpG_local_index_start1
    p2=CpG_local_index_start2
    b1=read_cpg_bin1
    b2=read_cpg_bin2
  else:
    p1=CpG_local_index_start2
    p2=CpG_local_index_start1
    b1=read_cpg_bin2
    b2=read_cpg_bin1
  L1=len(b1)
  L2=len(b2)
  d=p2-p1 # distance from p1 to p2
  if (L1-d)>=L2: # L1 contains L2
	b=b1[:d]+Merge(b1[d:(d+L2)],b2)+b1[(d+L2):] # merge L1 begining + overlapping of L1 & L2 + end of L1
	return (p1,b)
  if d<=L1: # two reads overlap
    b=b1[:d]+Merge(b1[d:],b2[:(L1-d)])+b2[(L1-d):] # L1 beginning + overlapping of L1 & L2 + end of L2
  else: # two reads no overlap
    b=b1+str(UNKOWN)*(d-L1)+b2 # L1 + unknown (not covered by sequencing reads) + L2
  return (p1,b)

def EpiPoly(bamfile,chrn,start,end,poslist):
  Nhomo=0
  Nhetero=0
  Nmeth=0
  Nunmeth=0 
  Lposlist=len(poslist)
  dmate={} # before merge into d_cpg_bin, read name is kept for merge read pairs
  d_cpg_bin={}
  samfile = pysam.Samfile( bamfile, "rb" )
  #result_list=[]
  #retrieve all reads that are mapped to the interval specified
  for alignedread in samfile.fetch( chrn,start,end):
    #print alignedread.qname
    # use only proper pair reads
    if alignedread.is_paired: # ok if single end read
      if not alignedread.is_proper_pair: # ok if proper pair
        if not alignedread.mate_is_unmapped: # ok if mate unmapped
          if not (alignedread.tid == alignedread.mrnm and alignedread.pos == alignedread.mpos):
            # ok if sharing same pos (bwa does not flag these as proper pairs)
            #print "continue"
            continue
    # skip aligned reads with indel
    # the only possible cigar is one unit with 0 or two unit with 0 and 4
    cigar=alignedread.cigar
    if cigar==None:
      #print "no cigar string"
      continue
    Lcigar=len(cigar)
    if Lcigar>2:
      #print "there is indel in the read"
      continue
    if Lcigar==2:
      if cigar[0][0]!=SOFTCLIP_CIGAR and cigar[1][0]!=SOFTCLIP_CIGAR:
        continue
    if ( alignedread.is_read1 and not alignedread.is_reverse) or ( alignedread.is_read2 and alignedread.is_reverse):
      #CTread=True
      cg_shift=0
    else:
      #CTread=False
      cg_shift=1
    (cg_ref_nt,cg_conv_nt)=d_cg_nt[cg_shift]
    # if the index is further than the maped reads
    if poslist[-1]<alignedread.pos:
     # print "the read is beyond the indexed CpG"  
      continue
    seq=alignedread.seq
    # find the first CpG in the read
    CpG_local_index=find_gt(poslist,alignedread.pos-1)
    # remember the starting local index, as the non-start one will be overwritten
    CpG_local_index_start=CpG_local_index
    # get the acutally genomic corrdinates given the local index
    CpGpos=poslist[CpG_local_index]
    # initiate an empty string to store the digital methylation information, such as '111202'
    read_cpg_bin=''
    loop=True
    # loop over all CpG that fell in the read region
    while loop and CpG_local_index<Lposlist:
      CpGpos=poslist[CpG_local_index]
      if CpGpos>=alignedread.aend:
        loop=False
        continue
      CpGpos=poslist[CpG_local_index]
      qpos=CpGpos-alignedread.pos+alignedread.qstart+cg_shift
      if qpos>=alignedread.qlen:
        break
      # CpG_nt=seq[qpos:qpos+2] # without addding cg_shift
      # CpG_bin is the value showing the methylation status 0 1 or 2 
      CpGqual=ord(alignedread.qual[qpos]) # check for base quality
      if CpGqual > (MIN_QUAL+33):
        CpGnt=seq[qpos]
        if CpGnt==cg_conv_nt: # 
          CpG_bin=UNMETH # non-methylated
          Nunmeth +=1
        elif CpGnt==cg_ref_nt:
          CpG_bin=METH # methylated
          Nmeth +=1
        else:
          CpG_bin=UNKOWN # no information
      else:
        CpG_bin=UNKOWN
      read_cpg_bin+=str(CpG_bin)
      CpG_local_index+=1
    if not dmate.has_key(alignedread.qname):
      dmate[alignedread.qname]=[]
    dmate[alignedread.qname].append((CpG_local_index_start,read_cpg_bin))
  for v in dmate.values():
    Lv=len(v)
    if Lv==1:
      (CpG_local_index_start,read_cpg_bin)=v[0]
    elif Lv==2:
      (CpG_local_index_start,read_cpg_bin)=MergeReadPair(v[0],v[1])
    else:
	print "error: more than two reads in a pair..."
    # if there are less than the number of minimal CpG sites in the read, then skip
    if len(read_cpg_bin)>=MIN_CPG:
      #print (alignedread.qname,CpG_local_index_start,read_cpg_bin)
      if not d_cpg_bin.has_key(read_cpg_bin):
        d_cpg_bin[read_cpg_bin]={}
      if not d_cpg_bin[read_cpg_bin].has_key(CpG_local_index_start):
        d_cpg_bin[read_cpg_bin][CpG_local_index_start]=0
      d_cpg_bin[read_cpg_bin][CpG_local_index_start] += 1
  return d_cpg_bin


def DiverstiyMeth(nlist,methlist):
	"""
	Calculate the Simpson's Diversity Indices, as in ecology
        http://www.countrysideinfo.co.uk/simpsons.htm
        if the coverage is less than MINcov, return -1
	nlist is the list of subpopulation sizes
	methlist is the list of methylation level per CpG in the loci
	"""
        nsum=sum(nlist) # total number of molecules
        if nsum<MINcov:
                return -1 # doesn't calculate, return -1
        meth=sum(methlist)/NCpG # the average methylation level of this loci
        n2sum=0 # the sum of the (n-1)*n for individual subpopulations
        for n in nlist:
                n2sum += n*(n-1)
        N2=nsum*(nsum-1) # (N-1)*N, where N is the population size 
        return (1-1.0*n2sum/N2,meth) 


def CalMeth(d_cpg_bin): # 4CpG
	"""
	calculate the average methylation level per CpG in the loci
	in this case, 4 continous CpGs
	if there is no covearge at any of the CpGs,return -1
	"""
	# here we assume there are maximal 4CpG in the locus which need to be revised for more flexibility
	nmeth=[0,0,0,0] # count the number of methylated CpG at this position
	ntotal=[0,0,0,0] # count the coverage at this position, i.e. sum of methylated and unmethylated
	meths=[]
	for pattern in d_cpg_bin.keys(): # pattern is e.g. '1122'
		if str(UNKOWN) in pattern: # if there is low quality base in it, exclude from calculation
			continue
		for index_start in d_cpg_bin[pattern].keys():
			n=d_cpg_bin[pattern][index_start] # n is the number of moleculues with that pattern starting from index_start
			i=index_start
			for p in pattern: # for each CpG within that pattern
				p=int(p)
				#if p==UNKOWN:
				#	continue 
				if p==METH:
					nmeth[i]+=n
				ntotal[i]+=n 
				i+=1
	for i in range(NCpG):
		if ntotal[i]==0:
			return -1
		else:
			meths.append(1.0*nmeth[i]/ntotal[i])
	return meths



#NCpG=4 # equal to MIN_CPG
NCpG=MIN_CPG

Npop=pow(2,NCpG)

chrn='chr'+str(chrn)
bamfile=SampleDIR+pid+'_temp/alignment/'+pid+'_merged.rmdup.bam'
ofile=outputDIR+pid+'_'+chrn+'_EpiScore_Meth.csv'
fout=open(ofile,'w')
if 1:
	if 1:
		ifile=inputDIR+pid+'_merged.CG.'+chrn+'.EpiInterval.csv'
		f=open(ifile)
		l=f.readline()
		for l in f:
			ls=l[:-1].split('\t')
			# read in the 4 positions
			pos1=int(float(ls[1]))
                        pos2=int(float(ls[2]))
                        pos3=int(float(ls[3]))
			pos4=int(float(ls[4]))
			poslist0=[pos1,pos2,pos3,pos4]
			# extend on both side in case 1 or 0-base confusion 
			start=poslist0[0]-2
        		end=poslist0[-1]+2
			#methlist=[float(ls[4]),float(ls[5]),float(ls[6])]
			# get the couts for all methylation patterns in this region
                	d_cpg_bin=EpiPoly(bamfile,chrn,start,end,poslist0)
			methlist=CalMeth(d_cpg_bin)
			if methlist==-1: # if there is no coverage at any CpG in the loci
				continue
			nlist=[]
			for k,x in d_cpg_bin.items(): # k is the meth pattern of subpopulation
				if str(UNKOWN) in k: # skip pattern with unkown base
					continue
				nlist.append(x[0]) # this assumes x is 
			# calcualte the diversity score
			result=DiverstiyMeth(nlist,methlist)
			if result!=-1:
				if result[1]<0.1:
					print d_cpg_bin,methlist,result
				fout.write(chrn+'\t'+str(pos1)+'\t'+str(result[0])+'\t'+str(result[1])+'\n')
	



fout.close()
