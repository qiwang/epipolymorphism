dmrtag=l_s_cg_dmr_t_sorted
dmrtag=AllDMR_3rep
for pid in LT ST MPP
	do
	for chrn in `seq 1 19`
		do
		DIR=/home/wangq/Daniel_TWGBS/result/results_per_pid/${pid}_temp/alignment
		inputfile=$DIR/${pid}_merged.epi.chr${chrn}.gz
		outputfile=$DIR/${pid}_merged.epi.${dmrtag}.chr${chrn}.gz
		gunzip -c $inputfile | awk -v chrn="chr10" '{printf  "%s\t%s\t%s\t%s\n",chrn,$1,$1+2,$2}' - | intersectBed -a - -b /home/wangq/Daniel_TWGBS/result/dmr/3_rep/bed/merged/${dmrtag}.bed | awk '{printf  "%s\t%s\n",$2,$4}' | gzip - >$outputfile
		done
	done
