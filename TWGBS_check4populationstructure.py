DIR='/home/wangq/Daniel_TWGBS/result/EpiPoly/'
filetag='MPP_chr1:54748216_54748252_minCpG_3_Epi'

MINcov=20

METH='1'
UNMETH='2'
UNKOWN='0'
# minmal fold change to report
MINfold=5


def Beta(methstatus,beta):
	if methstatus==METH:
		return beta
	elif methstatus==UNMETH:
		return 1-beta
	else:
		return -1



def Exp_pop(pop,listmeth):
	L=len(pop)
	if len(listmeth)!=L:
		print "error"
		return -1
	exp=1
	for i in range(L):
		exp = exp * Beta(pop[i],listmeth[i])		
	return exp

def CheckSubPopulation(filetag):
	# the population structure file
	popfile=DIR+filetag+'.csv'
	# the methylation file
	methfile=DIR+filetag+'.pos.meth'
	# read the population strucuture from file
	dpop={}
	fpop=open(popfile)
	for l in fpop:
        	ls=l[:-1].split('\t')
        	pop=ls[0]
        	if UNKOWN in pop:
                	continue
        	dpop[pop]=int(ls[2])
	fpop.close()
	# read methylation level from file
	fmeth=open(methfile)
	listmeth=[]
	for l in fmeth:
        	ls=l[:-1].split('\t')
	        m=int(ls[3])
        	u=int(ls[4])
        	t=m+u
        	if t<MINcov:
                	beta=-1
        	else:
                	beta=1.0*m/t
        	listmeth.append(beta)
	fmeth.close()
	# caluclate the expected and observed subpopulation frequencies
	Npop=sum(dpop.values())
	for pop,n in dpop.items():
		exp=Exp_pop(pop,listmeth)
	 	obs=1.0*n/Npop
	 	if exp/obs>MINfold:
			print filetag,pop,exp,obs,exp/obs



f=open(DIR+'csvlist')
for l in f:
	filetag=l.split('.')[0]
	CheckSubPopulation(filetag)
