"""
analysis work flow

1. load the CpG position of the whole chromosome: dpos[chrn]=poslist
2. interate over the reads in the bam file by pysam
3. for each read, first check whether its mate is already there
	if mate exist:
		if it does overlap with the current read:
			trim the read to no overlap: redefine start and end
			function to extract methylation status
 			extend the current read by (NCpG-1) CpG sites
			store 3CpG population structure in epi[chrn][pos]=[Npop1,...,Npopn]
		remove the overlapping mate to save memory
	else:
		function to extract methylation status
		store the position and methylation status of the two CpG sites for mate later (pos1, pos2,METH,UNMETH)
	add the population to the dictionary ExtractPop()
"""



# For each read, after extract info, it is like [CpGpos1,CpGpos2,...,CpGposm] ['1','0',...,'0']
def ExtractPop(poslist,methlist,dpopchr):
	M=len(poslist)
	if len(methlist)!=M:
		print "List not equal length!",poslist,methlist
		return -2
	for i in range(1,M-2):
		pos=poslist[i]
		if not dpopchr.has_key(pos):
			dpopchr[pos]=[0,0,0,0,0,0,0,0]
		pop=''
		for j in range(NCpG):
			p=methlist[i+j]
			if p==UNKOWN:
				break
			pop += p
		if p!=UNKOWN: # the previous loop is not interupted
			dpopchr[pos][POPINDEX[pop]] += 1
	return 0


def ExtractMeth(alignedread,poslist,cg_shift):
      	read_cpg_bin=''
        for CpGpos in poslist:
           	qpos=CpGpos-alignedread.pos+alignedread.qstart+cg_shift
                CpGqual=ord(alignedread.qual[qpos]) # check for base quality
               	if CpGqual < (MIN_BASEQUAL+33):
                     	CpG_bin=UNKOWN
            	else:
                    	CpGnt=seq[qpos]
                      	if CpGnt==cg_conv_nt: # 
                           	CpG_bin=UNMETH # non-methylated
                        elif CpGnt==cg_ref_nt:
                               	CpG_bin=METH # methylated
                      	else:
                            	CpG_bin=UNKOWN # no information
               	read_cpg_bin +=str(CpG_bin)
	return read_cpg_bin


def loadPositions(posfile,dpos):
	f=gzip.open(posfile)
	for l in f:
		ls=l[:-1].split('\t')
		if not dpos.has_key(ls[0]):
			dpos[ls[0]]=[]
		dpos[ls[0]].append(int(ls[1]))
	f.close()
	return


def PosInInterval(poslist,start,end):
	leftindex=bisect.bisect_left(poslist,start)
	rightindex=bisect.bisect_right(poslist,end)
	return poslist[leftindex:rightindex]
	

import gzip
import pysam
import bisect
import sys

METH='1'
UNMETH='2'
UNKOWN='0'

SOFTCLIP_CIGAR=4

MIN_MAPQUAL=20
MIN_BASEQUAL=20
TRIM=5 # trim the first and last 5nt
D_CG_NT={0:('C','T'),1:('G','A')}

OUTDIR='/home/wangq/Daniel_TWGBS/result/EpiPoly/'

NCpG=3
NCpGextend=NCpG-1
Npop=2^NCpG


POPINDEX={}
n=0
for x in ['1','2']:
        for y in ['1','2']:
                for z in ['1','2']:
                        pop=x+y+z
                        POPINDEX[pop]=n
                        n += 1


def CheckRead(alignedread):
	if alignedread.mapq<MIN_MAPQUAL:
         	return -1
        if alignedread.is_paired: # ok if single end read
                if not alignedread.is_proper_pair: # ok if proper pair
                	if not alignedread.mate_is_unmapped: # ok if mate unmapped
                            	if not (alignedread.tid == alignedread.mrnm and alignedread.pos == alignedread.mpos):
                                   	# ok if sharing same pos (bwa does not flag these as proper pairs)
                                       	#print "continue"
                                        return -1
    	# skip aligned reads with indel
      	# the only possible cigar is one unit with 0 or two unit with 0 and 4
      	cigar=alignedread.cigar
       	if cigar==None:
            	#print "no cigar string"
              	return -1
      	Lcigar=len(cigar)
       	if Lcigar>2:
             	#print "there is indel in the read"
        	return -1
       	if Lcigar==2:
               	if cigar[0][0]!=SOFTCLIP_CIGAR and cigar[1][0]!=SOFTCLIP_CIGAR:
                	return -1
	return 0


def WCstrand(alignedread):
	if ( alignedread.is_read1 and not alignedread.is_reverse) or ( alignedread.is_read2 and alignedread.is_reverse):
               	#CTread=True
              	cg_shift=0
     	else:
                #CTread=False
               	cg_shift=1
        return cg_shift



posfile='/icgc/lsdf/mb/analysis/Reference/mm10/methylCtools/mm10_PhiX_Lambda_CGonly_Index.pos.gz'
dpos={}
loadPositions(posfile,dpos)

pid=sys.argv[1] # e.g. "LT"
bamfile='/home/wangq/Daniel_TWGBS/result/results_per_pid/'+pid+'_temp/alignment/'+pid+'_merged.rmdup.bam'
outfile='/home/wangq/Daniel_TWGBS/result/results_per_pid/'+pid+'_temp/alignment/'+pid+'_merged.epi'

samfile = pysam.Samfile( bamfile, "rb" )
for chrN in range(1,20):
	chrn='chr'+str(chrN)
	fullposlist=dpos[chrn]
	dpop={}
	dmate={}
	fout=gzip.open(outfile+'.'+chrn+'.gz','wb')
	for alignedread in samfile.fetch(chrn):
		if CheckRead(alignedread)==-1:
        		continue
    		cg_shift=WCstrand(alignedread)
		(cg_ref_nt,cg_conv_nt)=D_CG_NT[cg_shift]
    		# if the index is further than the maped reads
		start=alignedread.pos+TRIM
		end=alignedread.aend-TRIM
		poslist=PosInInterval(fullposlist,start,end)
		if len(poslist)<NCpG:
			continue
    		seq=alignedread.seq
    		read_cpg_bin=ExtractMeth(alignedread,poslist,cg_shift)
		if dmate.has_key(alignedread.qname):
			mateinfo=dmate[alignedread.qname]
			del dmate[alignedread.qname]
			mateposlist=mateinfo[0]
			matecpglist=mateinfo[1]
			#print poslist,mateposlist
			#print read_cpg_bin,matecpglist
			if mateposlist[-1]<poslist[0] or mateposlist[0]>poslist[-1]:
				ExtractPop(poslist,read_cpg_bin,dpop)
				ExtractPop(mateposlist,matecpglist,dpop)
			else: # need to merge the two read
				if mateposlist[0]<poslist[0]:
					n=0
					for matepos in mateposlist:
						if matepos<poslist[0]:
							n=n+1
						else:
							break
					#print "extra",n
					poslist=mateposlist[:n]+poslist
					read_cpg_bin=matecpglist[:n]+read_cpg_bin
				elif mateposlist[-1]>poslist[-1]:
					n=0
                                        for matepos in mateposlist[::-1]:
                                               	if matepos>poslist[-1]:
                                                       	n=n+1
                                               	else:
                                                       	break
					#print "extra",n
                                        poslist=poslist+mateposlist[-n:]
                                        read_cpg_bin=read_cpg_bin+matecpglist[-n:]
			#print "merged",poslist
			ExtractPop(poslist,read_cpg_bin,dpop)
		else:
			if alignedread.is_reverse:
				dmate[alignedread.qname]=[poslist,read_cpg_bin]
 			else:
				dmate[alignedread.qname]=[poslist,read_cpg_bin]
	# process the unpaired read in dmate
	for (poslist,read_cpg_bin) in dmate.values():
		ExtractPop(poslist,read_cpg_bin,dpop)
	# write to output file
	for (pos,v) in dpop.items():
		fout.write(str(pos)+'\t')
		for vv in v:
			fout.write(str(vv)+',')
		fout.write('\n')
	fout.close()


samfile.close()
