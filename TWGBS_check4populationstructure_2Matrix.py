import math

DIR='/home/wangq/Daniel_TWGBS/result/EpiPoly/'

# minimal coverage per CpG sites to estimate the methylation level
MINcov=20 
# minimal size of the population with all 3 sites observed
MINpopsize=30

METH='1'
UNMETH='2'
UNKOWN='0'
# minmal fold change obs/exp to report
MINfold=3
POPSIZE=8 # 2^3

# given beta, return the probabilty of methylation (beta) or unmethylated(1-beta) 
def Beta(methstatus,beta):
	if methstatus==METH:
		return beta
	elif methstatus==UNMETH:
		return 1-beta
	else:
		return -1


# return the expected chance of observing the population "pop" given the beta of all sites
# assuming all sites are independent
def Exp_pop(pop,listmeth): # e.g. pop='121', listmeth=[0.5,0.3,0.7]
	L=len(pop)
	#print pop,listmeth
	if len(listmeth)!=L:
		print "error"
		return -1
	exp=1
	for i in range(L):
		exp = exp * Beta(pop[i],listmeth[i])		
	return exp


def GetSubPopulation(poplist,methlist):
        # if there is any position that have methylation value not estimate, this set skipped
        if '' in methlist:
                return -2
        dpop={}
	dresultx={}
        n=0
        if 'NA' in poplist or len(poplist)!=POPSIZE: # the size of the population is lower than limit, skip
                return -2
        for x in ['1','2']:
                for y in ['1','2']:
                        for z in ['1','2']:
                                popx=x+y+z
                                dpop[popx]=int(poplist[n])
                                n+=1
        Npop=sum(dpop.values())
        if Npop<MINpopsize:
                return -2
	for pop,n in dpop.items():
        	exp=Exp_pop(pop,methlist)
		obs=1.0*(n)/Npop
        	obs_robust=1.0*(n-ROBUST*math.sqrt(n))/Npop
		if exp!=0:
       			dresultx[pop]=(obs,exp,obs/exp,obs_robust/exp)
		else: # exp==0
			dresultx[pop]=(obs,exp,obs/ZERO,obs_robust/ZERO)
	return (Npop,dresultx)

"""
# detect subpopulation whose fequency is higher than expected assuming independency of sites
def CheckSubPopulation(poplist,methlist):
	# if there is any position that have methylation value not estimate, this set skipped
	if '' in methlist:
		return -2
	dpop={}
	n=0
	if 'NA' in poplist: # the size of the population is lower than limit, skip
		return -2
	for x in ['1','2']:
	        for y in ['1','2']:
        	        for z in ['1','2']:
                	        popx=x+y+z
                        	dpop[popx]=int(poplist[n])
				n+=1
	#print dpop
	# caluclate the expected and observed subpopulation frequencies
	Npop=sum(dpop.values())
	if Npop<MINpopsize:
		return -2
	temp=[]
	for pop,n in dpop.items():
		exp=Exp_pop(pop,methlist)
		obs=1.0*(n)/Npop
	 	obs_robust=1.0*(n-ROBUST*math.sqrt(n))/Npop
	 	if exp>0 and obs/exp>MINfold:
			temp.append((pop,Npop,obs,exp,obs/exp))
			print pop,Npop,obs,exp,obs/exp
	if temp!=[]:
		temp.append(('NS',Npop,'*','*','*'))
	return temp
"""			

# given ['2,8','2,8','2,8'], return [0.2,0.2,0.2]
def CalMeth(countlist):
	methlist=[]
	for t in countlist:
		ls=t.split(',')
		m=int(ls[0])
                u=int(ls[1])
                t=m+u
                if t<MINcov:
                        beta=''
                else:
                        beta=1.0*m/t
		methlist.append(beta)
	return methlist


#filetag='/home/wangq/Daniel_TWGBS/result/EpiPoly/LT_ST_MPP_PopulationCount'
filetag='/home/wangq/Daniel_TWGBS/result/EpiPoly/AllDMR_3rep_PopulationCount'
popfile=filetag+'.csv'
methfile=filetag+'.pos.meth.gz'

#M=3 # three different population
celltypes=['LT','ST','MPP']
NCELLTYPE=len(celltypes)
M=NCELLTYPE

import os

ROBUST=2 # N times the variance of possion distribution
ZERO=0.05 # when it is zero, the division would fail

dresult={}
f=open(popfile)
l=f.readline() # skip header
for l in f:
	ls=l[:-1].split('\t')
	region0=ls[0]
	chrn=region0.split(':')[0]
	startend=region0.split(':')[1].split('-')
	# since the previous step extend the interval, we shrink it a bit
	start=str(int(startend[0])+1)
	end=str(int(startend[1])-1)
	region=chrn+':'+start+'-'+end
	pops=ls[1:4] # LT,ST,MPP
	if len(pops)!=NCELLTYPE:
		print region,pops
		continue
	# tabix to get the methylation level for this region
	# sort meth level for three cell types
	dmethcount={} # dmethcount['LT']=['2,8','2,8','2,8']
	for celltype in celltypes:
		dmethcount[celltype]=[]
	command='tabix '+methfile+' '+region
        posindexresult=os.popen(command)
	n=0
        for l in posindexresult:
                ls=l[:-1].split('\t')
      		for i in range(M):
			dmethcount[celltypes[i]].append(ls[i+3]) 
		n=n+1     
        posindexresult.close()
	if n!=3: # 3CpG per interval
		print "error",region,n
		break
	dresult3={}
	for i in range(M) :
		celltype=celltypes[i]
		methlist=CalMeth(dmethcount[celltype])	
		result=GetSubPopulation(pops[i].split(','),methlist)
		if result==-2: # population size not big enough
			continue
		dresult3[celltype]=result
		(Npop,dresultx)=result
		for pop,v in dresultx.items():
			if v[3]<MINfold:
				continue
			if not dresult.has_key(region):
				dresult[region]={}
			if not dresult[region].has_key(pop):
				dresult[region][pop]={}
			for tresult in result:
				dresult[region][pop][celltype]=v
	if not dresult.has_key(region):
		continue
	for pop in dresult[region].keys():
		for j in range(M) :
	        	celltype=celltypes[j]
			if not dresult[region][pop].has_key(celltype) and dresult3.has_key(celltype):
				dresult[region][pop][celltype]=dresult3[celltype][1][pop]

		


#dcount={'LT':0,'ST':0,'MPP':0}
#dt={'LT':0,'ST':0,'MPP':0}
#dp={'LT':[],'ST':[],'MPP':[]}

fout=open(filetag+'_result_June4.csv','w')
for r in dresult.keys():
	for pop in dresult[r].keys():
		for c in celltypes:
			if not dresult[r][pop].has_key(c):
				continue
			v=dresult[r][pop][c]	
			#dp[c].append(v[1])
			fout.write(r+'\t'+c+'\t'+pop+'\t'+str(round(v[0],3))+'\t'+str(round(v[1],3))+'\t'+str(round(v[2],3))+'\t'+str(round(v[3],3))+'\n')
			#print r,pop,c,dresult[r][pop][c]
	#print ''
	fout.write('\n')


fout.close()

"""
for c in celltypes:
	print c,dcount[c],dt[c],1.0*dcount[c]/dt[c]


# minimal population size 30

LT 2425 4522 0.536267138434
ST 1683 3319 0.507080445917
MPP 1556 3825 0.406797385621
"""
