# In the previous version, the most significant subpopulations are identified
# the 2nd subpopulation should be calculated based the remaining population after the first one have been identified, 
# so the expected methylation level should be adjusted by the most significant population and its estimated frequency

#p(pop1) * beta1 + p(pop2) * beta2 = beta
#beta2= (beta-p(pop1)*beta1)/(1-p(pop1))

DIR='/home/wangq/Daniel_TWGBS/result/EpiPoly/'

# minimal coverage per CpG sites to estimate the methylation level
MINcov=20 
# minimal size of the population with all 3 sites observed
MINpopsize=30

METH='1'
UNMETH='2'
UNKOWN='0'
# minmal fold change obs/exp to report
MINfold=3


# given beta, return the probabilty of methylation (beta) or unmethylated(1-beta) 
def Beta(methstatus,beta):
	if methstatus==METH:
		return beta
	elif methstatus==UNMETH:
		return 1-beta
	else:
		return -1


# return the expected chance of observing the population "pop" given the beta of all sites
# assuming all sites are independent
def Exp_pop(pop,listmeth): # e.g. pop='121', listmeth=[0.5,0.3,0.7]
	L=len(pop)
	#print pop,listmeth
	if len(listmeth)!=L:
		print "error"
		return -1
	exp=1
	for i in range(L):
		exp = exp * Beta(pop[i],listmeth[i])		
	return exp


# detect subpopulation whose fequency is higher than expected assuming independency of sites
def CheckSubPopulation(poplist,methlist,dfreq):
	# if there is any position that have methylation value not estimate, this set skipped
	if '' in methlist:
		return -2
	dpop={}
	n=0
	if 'NA' in poplist: # the size of the population is lower than limit, skip
		return -2
	for x in ['1','2']:
	        for y in ['1','2']:
        	        for z in ['1','2']:
                	        popx=x+y+z
                        	dpop[popx]=int(poplist[n])
				n+=1
	#print dpop
	# caluclate the expected and observed subpopulation frequencies
	Npop=sum(dpop.values())
	if Npop<MINpopsize:
		return -2
	temp=[]
	for pop,n in dpop.items():
		exp=Exp_pop(pop,methlist)
		obs=1.0*(n)/Npop
	 	obs_robust=1.0*(n-ROBUST)/Npop
		#print pop,Npop,exp,obs,round(exp/(obs+0.05),2)
	 	if exp>0 and obs_robust/exp>MINfold:
			temp.append((pop,Npop,obs,exp,obs_robust/exp))
			print pop,Npop,obs,exp,obs/exp
		dfreq[pop]=(obs,exp)
	if temp==[]:
		temp.append(('NS',Npop,'*','*','*'))
	return temp
			

# given ['2,8','2,8','2,8'], return [0.2,0.2,0.2]
def CalMeth(countlist):
	methlist=[]
	for t in countlist:
		ls=t.split(',')
		m=int(ls[0])
                u=int(ls[1])
                t=m+u
                if t<MINcov:
                        beta=''
                else:
                        beta=1.0*m/t
		methlist.append(beta)
	return methlist

	
def METHbeta(status):
	if status==METH:
		return 1
	elif status==UNMETH:
		return 0
	else:
		return -1


def AdjustMeth(methlist,dfreqpop): # dfreqpop={'111':0.1,'122':0.3'} # beta2= (beta-p(pop1)*beta1)/(1-p(pop1))
	L=len(methlist)
	adjusted_meth_list=[]
	for i in range(L):
		tmeth=methlist[i]
		tpop=1
		for pop,f in dfreqpop.items():
			tbeta=METHbeta(pop[i])
			tmeth= tmeth - f*tbeta
			tpop= tpop - f
		adjusted_meth=tmeth/tpop
		adjusted_meth_list.append(adjusted_meth)
	return adjusted_meth_list






filetag='/home/wangq/Daniel_TWGBS/result/EpiPoly/LT_ST_MPP_PopulationCount'
popfile=filetag+'.csv'
methfile=filetag+'.pos.meth.gz'

M=3 # three different population
celltypes=['LT','ST','MPP']

import os

ROBUST=1 # make the observation one less as a conservative estimate

dresult={}
dresult2={}
dresult3={}

f=open(popfile)
l=f.readline() # skip header
for l in f:
	ls=l[:-1].split('\t')
	region0=ls[0]
	chrn=region0.split(':')[0]
	startend=region0.split(':')[1].split('-')
	# since the previous step extend the interval, we shrink it a bit
	start=str(int(startend[0])+1)
	end=str(int(startend[1])-1)
	region=chrn+':'+start+'-'+end
	pops=ls[1:4] # LT,ST,MPP
	# tabix to get the methylation level for this region
	# sort meth level for three cell types
	dmethcount={} # dmethcount['LT']=['2,8','2,8','2,8']
	for celltype in celltypes:
		dmethcount[celltype]=[]
	command='tabix '+methfile+' '+region
        posindexresult=os.popen(command)
	n=0
        for l in posindexresult:
                ls=l[:-1].split('\t')
      		for i in range(M):
			dmethcount[celltypes[i]].append(ls[i+3]) 
		n=n+1     
        posindexresult.close()
	if n!=3: # 3CpG per interval
		print "error",dmethcount
		break
	for i in range(M) :
		dx={}
		dy={}
		dz={}
		celltype=celltypes[i]
		methlist=CalMeth(dmethcount[celltype])	
		result=CheckSubPopulation(pops[i].split(','),methlist,dx)
		if result==-2: # population size not big enough
			continue
                #print 'x',dx
                print dx['111'],dx['222']

		#if result==-1: # no subpopulation significantly deviate from expected
		#	result=[['NS','*','*','*','*']]
		#print celltype,region,result
		if not dresult.has_key(region):
			dresult[region]={}
		if not dresult[region].has_key(celltype):
			dresult[region][celltype]={}
		dfreqpop={} # this dictionary store the frquency of the significant subpopulation
		for tresult in result:
			dresult[region][celltype][tresult[0]]=tresult[1:5]
			if tresult[0]!='NS':
				dfreqpop[tresult[0]]= tresult[2]
		# this is 2nd round of population selection, after eliminating the dominant ones
		if result[0][0]=='NS': # the 1st round of selection can't be empty
			continue
		adjusted_meth_list=AdjustMeth(methlist,dfreqpop)
		result2=CheckSubPopulation(pops[i].split(','),adjusted_meth_list,dy)
		#print 'y',dy
		if result2==-2: # population size not big enough
			continue
		if not dresult2.has_key(region):
			dresult2[region]={}
		if not dresult2[region].has_key(celltype):
			dresult2[region][celltype]={}
		for tresult2 in result2:
			# skip if already identified in the first round
			if dresult[region][celltype].has_key(tresult2[0]):
				continue
			dresult2[region][celltype][tresult2[0]]=tresult2[1:5]
			dfreqpop[tresult2[0]]= tresult2[2]
		# the 3rd round of selection
		# skip if there is no significant subpopulation
		if result2[0][0]=='NS':
			continue
		adjusted_meth_list=AdjustMeth(methlist,dfreqpop)
                result3=CheckSubPopulation(pops[i].split(','),adjusted_meth_list,dz)
		#print 'z',dz
                if result3==-2: # population size not big enough
                        continue
                if not dresult3.has_key(region):
                        dresult3[region]={}
                if not dresult3[region].has_key(celltype):
                        dresult3[region][celltype]={}
                for tresult3 in result3:
                        # skip if already identified in the first round
                        if dresult[region][celltype].has_key(tresult3[0]):
                                continue
			if dresult2[region][celltype].has_key(tresult3[0]):
                                continue
                        dresult3[region][celltype][tresult3[0]]=tresult3[1:5]
                        



dcount={'LT':0,'ST':0,'MPP':0}
dcount2={'LT':0,'ST':0,'MPP':0}
dcount3={'LT':0,'ST':0,'MPP':0}

dt={'LT':0,'ST':0,'MPP':0}
dp={'LT':[],'ST':[],'MPP':[]}

# write the result in three difference level of substructure
fout1=open(DIR+'EpiSubpopulation_result1.csv','w')
fout2=open(DIR+'EpiSubpopulation_result2.csv','w')
fout3=open(DIR+'EpiSubpopulation_result3.csv','w')
for r in dresult.keys():
	for c in dresult[r].keys():
		dt[c]+=1
		if 'NS' in dresult[r][c].keys() or len(dresult[r][c])==0:
			continue
		dcount[c]+=1
		for pop,v in dresult[r][c].items():	
			dp[c].append(v[1])
			fout.write(r+'\t'+c+'\t'+pop+'\t'+str(v[0])+'\t'+str(v[1])+'\t'+str(v[2])+'\t'+str(v[3])+'\n')
                if 'NS' in dresult2[r][c].keys() or len(dresult2[r][c])==0:
                        continue
		#	print r,c,dresult[r][c]  
		#	print '----',dresult2[r][c]
		dcount2[c]+=1
		#	for pop,v in dresult2[r][c].items():
		#		fout.write(r+'\t'+c+'_sub'+'\t'+pop+'\t'+str(v[0])+'\t'+str(v[1])+'\t'+str(v[2])+'\t'+str(v[3])+'\n')
		if 'NS' in dresult3[r][c].keys() or len(dresult3[r][c])==0:
                        continue
                print r,c,dresult[r][c]
                print '----',dresult2[r][c]
		print '>>>>>',dresult3[r][c]
               	dcount3[c]+=1
                        #for pop,v in dresult2[r][c].items():
                        #        fout.write(r+'\t'+c+'_sub'+'\t'+pop+'\t'+str(v[0])+'\t'+str(v[1])+'\t'+str(v[2])+'\t'+str(v[3])+'\n')


for c in celltypes:
	print c,dcount[c],dcount[c],dcount2[c],dcount3[c],dt[c],1.0*dcount[c]/dt[c],1.0*dcount2[c]/dt[c],1.0*dcount3[c]/dt[c]


# minimal population size 30

LT 2425 4522 0.536267138434
ST 1683 3319 0.507080445917
MPP 1556 3825 0.406797385621

