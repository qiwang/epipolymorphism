@ /home/wangq/Daniel_TWGBS/result/EpiPoly/AllRegion/EpiScore_Meth
for pid in LT ST MPP2 MPP; do cat ${pid}EpiScore_Meth.csv | sort -k1,1 -k2,2n | bgzip >${pid}_EpiScore_Meth_sorted.bgzip; tabix -s 1 -b 2 -e 2 ${pid}_EpiScore_Meth_sorted.bgzip; done


chr1    168417089       168417123       PBX1
chr2    28606031        28606195        GFI1B
chr12   12909791        12909687        MYCN
chr12   12909840        12909752        MYCN
chr3    30219523        30219704        MECOM
chr17   27637595        27637651        RPS10


wangq@tbi-dsx01: EpiScore_Meth$ tabix LT_EpiScore_Meth_sorted.bgzip chr1:168417089-168417123
wangq@tbi-dsx01: EpiScore_Meth$ tabix LT_EpiScore_Meth_sorted.bgzip chr2:28606031-28606195
chr2    28606070        0.870967741935  0.53125
wangq@tbi-dsx01: EpiScore_Meth$ tabix LT_EpiScore_Meth_sorted.bgzip chr12:12909791-12909687
wangq@tbi-dsx01: EpiScore_Meth$ tabix LT_EpiScore_Meth_sorted.bgzip chr12:12909840-12909752
wangq@tbi-dsx01: EpiScore_Meth$ tabix LT_EpiScore_Meth_sorted.bgzip chr3:30219523-30219704
chr3    30219620        0.695195195195  0.378378378378
chr3    30219628        0.664102564103  0.358333333333
chr3    30219635        0.629129129129  0.378378378378
wangq@tbi-dsx01: EpiScore_Meth$ tabix LT_EpiScore_Meth_sorted.bgzip chr17:27637595-27637651
chr17   27637604        0.677419354839  0.520833333333



