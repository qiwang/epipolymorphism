regions='chr6:88190532-88190741 chr17:27637595-27637651 chr3:30219523-30219704 chr12:12909840-12909752 chr12:12909791-12909687 chr2:28606031-28606195 chr1:168417089-168417123 chr6:88190338-88190532 chr6:88190532-88190741 chr6:88190741-88191157'

regions='chr1:168416751-168417225 chr2:28605633-28606762 chr12:12909691-12909950 chr4:117303956-117304974 chr4:117288302-117288601'

regions='chr4:117304426-117305030 chr4:117304225-117304426 chr4:117303868-117304225'
DIR=/home/wangq/Daniel_TWGBS/result/EpiPoly/AllRegion/EpiScore_Meth
DIRout=$DIR/CandidateRegion

for region in $regions
	do
	outfile=$DIRout/${region}_EpiScore_4Pop.csv
	touch $outfile
	for pid in LT ST MPP2 MPP
		do
		tabix $DIR/${pid}_EpiScore_Meth_sorted.bgzip $region | awk -v pid=$pid '{printf "%s\t%s\t%s\n",pid,$3,$4}' >>$outfile
	done
done
