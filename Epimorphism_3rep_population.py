METH=1
UNMETH=2
UNKOWN=0

OUTDIR='/home/wangq/Daniel_TWGBS/result/EpiPoly/'

# output is the 012 string representing the reads where all 3 sites are covered
# the methylation level for individual CpG can be derived independently in a different function

def EpiPoly(bamfile,chrn,start,end,poslist):
  Nhomo=0
  Nhetero=0
  Nmeth=0
  Nunmeth=0
  Lposlist=len(poslist)
  d_cpg_bin={}
  samfile = pysam.Samfile( bamfile, "rb" )
  #result_list=[]
  #retrieve all reads that are mapped to the interval specified
  for alignedread in samfile.fetch( chrn,start,end):
    print alignedread.qname
    # use only proper pair reads
    if alignedread.is_paired: # ok if single end read
      if not alignedread.is_proper_pair: # ok if proper pair
        if not alignedread.mate_is_unmapped: # ok if mate unmapped
          if not (alignedread.tid == alignedread.mrnm and alignedread.pos == alignedread.mpos):
            # ok if sharing same pos (bwa does not flag these as proper pairs)
            print "continue"
            continue
    # skip aligned reads with indel
    # the only possible cigar is one unit with 0 or two unit with 0 and 4
    cigar=alignedread.cigar
    if cigar==None:
      print "no cigar string"
      continue
    Lcigar=len(cigar)
    if Lcigar>2:
      print "there is indel in the read"
      continue
    if Lcigar==2:
      if cigar[0][0]!=SOFTCLIP_CIGAR and cigar[1][0]!=SOFTCLIP_CIGAR:
        continue
    if ( alignedread.is_read1 and not alignedread.is_reverse) or ( alignedread.is_read2 and alignedread.is_reverse):
      #CTread=True
      cg_shift=0
    else:
      #CTread=False
      cg_shift=1
    (cg_ref_nt,cg_conv_nt)=d_cg_nt[cg_shift]
    # if the index is further than the maped reads
    if poslist[-1]<alignedread.pos:
      print "the read is beyond the indexed CpG"  
      continue
    seq=alignedread.seq
    # find the first CpG in the read
    CpG_local_index=find_gt(poslist,alignedread.pos-1)
    # remember the starting local index, as the non-start one will be overwritten
    CpG_local_index_start=CpG_local_index
    # get the acutally genomic corrdinates given the local index
    CpGpos=poslist[CpG_local_index]
    # initiate an empty string to store the digital methylation information, such as '111202'
    read_cpg_bin=''
    loop=True
    # loop over all CpG that fell in the read region
    while loop and CpG_local_index<Lposlist:
      CpGpos=poslist[CpG_local_index]
      if CpGpos>=alignedread.aend:
        loop=False
        continue
      CpGpos=poslist[CpG_local_index]
      qpos=CpGpos-alignedread.pos+alignedread.qstart+cg_shift
      if qpos>=alignedread.qlen:
        break
      # CpG_nt=seq[qpos:qpos+2] # without addding cg_shift
      # CpG_bin is the value showing the methylation status 0 1 or 2 
      CpGqual=ord(alignedread.qual[qpos]) # check for base quality
      if CpGqual > (MIN_QUAL+33):
        CpGnt=seq[qpos]
        if CpGnt==cg_conv_nt: # 
          CpG_bin=UNMETH # non-methylated
          Nunmeth +=1
        elif CpGnt==cg_ref_nt:
          CpG_bin=METH # methylated
          Nmeth +=1
        else:
          CpG_bin=UNKOWN # no information
      else:
        CpG_bin=UNKOWN
      read_cpg_bin+=str(CpG_bin)
      CpG_local_index+=1
    # if there are less than the number of minimal CpG sites in the read, then skip
    if len(read_cpg_bin)>=MIN_CPG:                 
      print (alignedread.qname,CpG_local_index_start,read_cpg_bin)
      if not d_cpg_bin.has_key(read_cpg_bin):
        d_cpg_bin[read_cpg_bin]={}
      if not d_cpg_bin[read_cpg_bin].has_key(CpG_local_index_start):
        d_cpg_bin[read_cpg_bin][CpG_local_index_start]=0
      d_cpg_bin[read_cpg_bin][CpG_local_index_start] += 1
  return d_cpg_bin
 
        

import pysam
import os
import bisect
import sys

####### functions #########################
def find_gt(a, x):
  'Find leftmost value greater than x'
  i = bisect.bisect_right(a, x)
  if i != len(a):
    return i
  raise ValueError
    
######### paramters ################
PAD=10
SOFTCLIP_CIGAR=4
posfile='/icgc/lsdf/mb/analysis/Reference/mm10/methylCtools/mm10_PhiX_Lambda_CGonly_Index.pos.gz'
MIN_QUAL=20 # minimal base quality
 
MIN_PAIR=20 
########## loop over read in this region ########
d_cg_nt={0:('C','T'),1:('G','A')}

def merge_d(d1,d2): # merged d2 into d1
  for k1 in d2.keys():
    if not d1.has_key(k1):
      d1[k1]={}
    for k2 in d2[k1].keys():
      if not d1[k1].has_key(k2):
        d1[k1][k2]=d2[k1][k2]
      else:
        d1[k1][k2]+=d2[k1][k2]
  

def sort_by_mean(ks):
  d={}
  for k in ks:
    n=0
    for kk in k:
      n=n+int(kk)
    m=1.0*n/len(k)
    if not d.has_key(m):
      d[m]=[k]
    else:
      d[m].append(k)
  l=[]
  ks=d.keys()
  ks.sort()
  for k in ks:
    for x in d[k]:
      l.append(x)
  return l
  
  
MIN_CPG=3 # minimal number of CpGs per read to be plotted


SampleGroupList=[['LT'],['ST'],['MPP2'],['MPP']]

MINcov=30

def PrepareCSV4Epi(chrn,start,end):
	# prepare the list of positions and store it in poslist
	command='tabix '+posfile+' '+chrn+':'+str(start)+'-'+str(end)
	posindexresult=os.popen(command)
	l=posindexresult.readline()
	ls=l[:-1].split('\t')
	posindex_start=int(ls[2])
	poslist0=[int(ls[1])]
	for l in posindexresult:
  		ls=l[:-1].split('\t')
  		poslist0.append(int(ls[1]))
	posindexresult.close()
	# make a loop to get every 3 continous cpg 
	Lpos=len(poslist0)
	for i in range(0,Lpos-2):
		poslist=poslist0[i:i+3]
	start3cpg=min(poslist)-1
	end3cpg=max(poslist)+2
	# read bam file and extract information per read
	for pids in SampleGroupList:
		# prepare empty string
  		x=[]
  		y=[]
  		pch_list=[]
  		i=0
  		d_cpg_bin={}
  		for pid in pids:
    			DIR='/home/wangq/Daniel_TWGBS/result/results_per_pid/'+pid+'_temp/alignment/'
    			bamfile=DIR+pid+'_merged.rmdup.bam'
			# call main function
    			d_cpg_bin_x=EpiPoly(bamfile,chrn,start3cpg,end3cpg,poslist)
    			merge_d(d_cpg_bin,d_cpg_bin_x)
		n=0
		for t in d_cpg_bin.values():
			n+=sum(t.values())
		if n<MINcov:
			continue
		fout=open(OUTDIR+'/'+pid+'_'+chrn+':'+str(start3cpg)+'_'+str(end3cpg)+'_minCpG_'+str(MIN_CPG)+'_Epi.csv','w')
		fout2=open(OUTDIR+'/'+pid+'_'+chrn+':'+str(start3cpg)+'_'+str(end3cpg)+'_minCpG_'+str(MIN_CPG)+'_Epi.pos','w')
		for t in poslist:
			fout2.write(chrn+'\t'+str(t)+'\t'+str(t+1)+'\n')
  		for read_cpg_bin in sort_by_mean(d_cpg_bin.keys()):
    			for CpG_local_index_start in d_cpg_bin[read_cpg_bin].keys():
      				n=d_cpg_bin[read_cpg_bin][CpG_local_index_start]
				# write CSV file
    			fout.write(read_cpg_bin+'\t'+str(CpG_local_index_start)+'\t'+str(n)+'\n')
		fout.close()
		fout2.close()


bedlistfile='/home/wangq/Daniel_TWGBS/result/dmr/3_rep/bed/merged/l_s_cg_dmr_t_EndAdd1.bed'
f=open(bedlistfile)
for l in f:
	ls=l[:-1].split('\t')
	chrn=ls[0]
	start0=int(ls[1])
	end0=int(ls[2])
	start=start0-PAD
	end=end0+PAD
	PrepareCSV4Epi(chrn,start,end)

f.close()

chrn='chr6'
start=88190150
end=88193482
PrepareCSV4Epi(chrn,start,end)
