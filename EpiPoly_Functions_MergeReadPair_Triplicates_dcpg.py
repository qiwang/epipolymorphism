import pysam
import os
import bisect
import sys

######### paramters ################
PAD=10
#posfile='/ibios/temp1/wangq/mm10_PhiX_Lambda_CGonly_Index.pos.bgz'
MIN_QUAL=20 # minimal base quality

MIN_CPG=3 # minimal number of CpGs per read to be plotted
SampleList=['LT','ST','MPP2','MPP']

MINcov=30

############## constant defined
METH=1
UNMETH=2
UNKOWN=0
dMETHSymb={'000':'222','100':'122','010':'212','001':'221','110':'112','101':'121','011':'211','111':'111'} # for easy interpretation
posfile='/icgc/ngs_share/assemblies/mm10/indexes/methylCtools/methylCtools_mm10_UCSC/mm10_PhiX_Lambda_CGonly_Index.pos.gz'

SOFTCLIP_CIGAR=4

########## loop over read in this region ########
d_cg_nt={0:('C','T'),1:('G','A')}

##### functions #################33
def find_gt(a, x):
  'Find leftmost value greater than x'
  i = bisect.bisect_right(a, x)
  if i != len(a):
    return i
  raise ValueError

def merge_d(d1,d2): # merged d2 into d1
  for k1 in d2.keys():
    if not d1.has_key(k1):
      d1[k1]={}
    for k2 in d2[k1].keys():
      if not d1[k1].has_key(k2):
        d1[k1][k2]=d2[k1][k2]
      else:
        d1[k1][k2]+=d2[k1][k2]


def sort_by_mean(ks):
  d={}
  for k in ks:
    n=0
    for kk in k:
      if kk=='0':
	n=n+1.5
      else:
      	n=n+int(kk)
    m=1.0*n/len(k)
    if not d.has_key(m):
      d[m]=[k]
    else:
      d[m].append(k)
  l=[]
  ks=d.keys()
  ks.sort()
  for k in ks:
    for x in d[k]:
      l.append(x)
  return l


def Merge(s1,s2):
  sUNKOWN=str(UNKOWN)
  L1=len(s1)
  if len(s2)!=L1:
    return -1
  s=''
  for i in range(L1):
    if s1[i]==s2[i]:
      s=s+s1[i]
    elif s1[i]==sUNKOWN:
      s=s+s2[i]
    elif s2[i]==sUNKOWN:
      s=s+s1[i]
    else:
      s=s+sUNKOWN
  return s

	
def MergeReadPair(v1,v2):
  #print v1,v2
  (CpG_local_index_start1,read_cpg_bin1)=v1
  (CpG_local_index_start2,read_cpg_bin2)=v2
  if CpG_local_index_start1<=CpG_local_index_start2:
    p1=CpG_local_index_start1
    p2=CpG_local_index_start2
    b1=read_cpg_bin1
    b2=read_cpg_bin2
  else:
    p1=CpG_local_index_start2
    p2=CpG_local_index_start1
    b1=read_cpg_bin2
    b2=read_cpg_bin1
  L1=len(b1)
  L2=len(b2)
  d=p2-p1
  if (L1-d)>=L2: # L1 contains L2
	b=b1[:d]+Merge(b1[d:(d+L2)],b2)+b1[(d+L2):]
	return (p1,b)
  if d<=L1: # two reads overlap
    b=b1[:d]+Merge(b1[d:],b2[:(L1-d)])+b2[(L1-d):]
  else:
    b=b1+str(UNKOWN)*(d-L1)+b2
  return (p1,b)

def EpiPoly(bamfile,chrn,start,end,poslist):
  Nhomo=0
  Nhetero=0
  Nmeth=0
  Nunmeth=0
  Lposlist=len(poslist)
  dmate={} # before merge into d_cpg_bin, read name is kept for merge read pairs
  d_cpg_bin={}
  samfile = pysam.Samfile( bamfile, "rb" )
  #result_list=[]
  #retrieve all reads that are mapped to the interval specified
  for alignedread in samfile.fetch( chrn,start,end):
    #print alignedread.qname
    # use only proper pair reads
    if alignedread.is_paired: # ok if single end read
      if not alignedread.is_proper_pair: # ok if proper pair
        if not alignedread.mate_is_unmapped: # ok if mate unmapped
          if not (alignedread.tid == alignedread.mrnm and alignedread.pos == alignedread.mpos):
            # ok if sharing same pos (bwa does not flag these as proper pairs)
            #print "continue"
            continue
    # skip aligned reads with indel
    # the only possible cigar is one unit with 0 or two unit with 0 and 4
    cigar=alignedread.cigar
    if cigar==None:
      #print "no cigar string"
      continue
    Lcigar=len(cigar)
    if Lcigar>2:
      #print "there is indel in the read"
      continue
    if Lcigar==2:
      if cigar[0][0]!=SOFTCLIP_CIGAR and cigar[1][0]!=SOFTCLIP_CIGAR:
        continue
    if ( alignedread.is_read1 and not alignedread.is_reverse) or ( alignedread.is_read2 and alignedread.is_reverse):
      #CTread=True
      cg_shift=0
    else:
      #CTread=False
      cg_shift=1
    (cg_ref_nt,cg_conv_nt)=d_cg_nt[cg_shift]
    # if the index is further than the maped reads
    if poslist[-1]<alignedread.pos:
     # print "the read is beyond the indexed CpG"  
      continue
    seq=alignedread.seq
    # find the first CpG in the read
    CpG_local_index=find_gt(poslist,alignedread.pos-1)
    # remember the starting local index, as the non-start one will be overwritten
    CpG_local_index_start=CpG_local_index
    # get the acutally genomic corrdinates given the local index
    CpGpos=poslist[CpG_local_index]
    # initiate an empty string to store the digital methylation information, such as '111202'
    read_cpg_bin=''
    loop=True
    # loop over all CpG that fell in the read region
    while loop and CpG_local_index<Lposlist:
      CpGpos=poslist[CpG_local_index]
      if CpGpos>=alignedread.aend:
        loop=False
        continue
      CpGpos=poslist[CpG_local_index]
      qpos=CpGpos-alignedread.pos+alignedread.qstart+cg_shift
      if qpos>=alignedread.qlen:
        break
      # CpG_nt=seq[qpos:qpos+2] # without addding cg_shift
      # CpG_bin is the value showing the methylation status 0 1 or 2 
      CpGqual=ord(alignedread.qual[qpos]) # check for base quality
      if CpGqual > (MIN_QUAL+33):
        CpGnt=seq[qpos]
        if CpGnt==cg_conv_nt: # 
          CpG_bin=UNMETH # non-methylated
          Nunmeth +=1
        elif CpGnt==cg_ref_nt:
          CpG_bin=METH # methylated
          Nmeth +=1
        else:
          CpG_bin=UNKOWN # no information
      else:
        CpG_bin=UNKOWN
      read_cpg_bin+=str(CpG_bin)
      CpG_local_index+=1
    if not dmate.has_key(alignedread.qname):
      dmate[alignedread.qname]=[]
    dmate[alignedread.qname].append((CpG_local_index_start,read_cpg_bin))
  for v in dmate.values():
    Lv=len(v)
    if Lv==1:
      (CpG_local_index_start,read_cpg_bin)=v[0]
    elif Lv==2:
      (CpG_local_index_start,read_cpg_bin)=MergeReadPair(v[0],v[1])
    else:
	print "error: more than two reads in a pair..."
    # if there are less than the number of minimal CpG sites in the read, then skip
    if len(read_cpg_bin)>=MIN_CPG:
      #print (alignedread.qname,CpG_local_index_start,read_cpg_bin)
      if not d_cpg_bin.has_key(read_cpg_bin):
        d_cpg_bin[read_cpg_bin]={}
      if not d_cpg_bin[read_cpg_bin].has_key(CpG_local_index_start):
        d_cpg_bin[read_cpg_bin][CpG_local_index_start]=0
      d_cpg_bin[read_cpg_bin][CpG_local_index_start] += 1
  return d_cpg_bin

MinCov=20

def Generate_dcpg_TriCpG(chrn,start,end,gene):
	#fout=open('/home/wangq/METH/Daniel_Mm/report/ManuscriptFigure/Fig7/EightPop/'+gene+'_'+str(start)+'_'+str(end)+'_EightPop.csv','w')
        poslist0=[]
        command='tabix '+posfile+' '+chrn+':'+str(start)+'-'+str(end)
	print command
        posindexresult=os.popen(command)
        l=posindexresult.readline()
        ls=l[:-1].split('\t')
        posindex_start=int(ls[2])
        poslist0=[int(ls[1])]
        for l in posindexresult:
                ls=l[:-1].split('\t')
                poslist0.append(int(ls[1]))
        posindexresult.close()
	N=len(poslist0)
	for i in range(N-2):
		poslist=poslist0[i:(i+3)]
		coverageTAG=True
		D={}
	        for pid in SampleList:
        	        bamfile='/home/wangq/Daniel_TWGBS/result/results_per_pid/'+pid+'_temp/alignment/'+pid+'_merged.rmdup.bam'
                	d_cpg_bin=EpiPoly(bamfile,chrn,start,end,poslist)
			D[pid]={}
			N=0
			for k,v in d_cpg_bin.items():
				if '0' in k: # there is unknown base
					continue
				N+=v[0]
				D[pid][k]=v[0]
			if N<MinCov:
				coverageTAG=False
		if coverageTAG:
			fout=open('/home/wangq/METH/Daniel_Mm/report/ManuscriptFigure/Fig7/EightPop/'+gene+'_'+str(poslist0[i])+'_EightPop.csv','w')
			#fout.write(str(poslist[0])+'_'+str(poslist[1])+'_'+str(poslist[2])+'\n')
			fout.write('\tHSC\tMPP1\tMPP2\tMPP3+4\n')
			for k0 in ['000','100','010','001','110','101','011','111']:
				fout.write('x'+k0+'\t')
				k=dMETHSymb[k0]
				for pid in SampleList:
					if not D[pid].has_key(k):
						fout.write('0\t')
					else:
						fout.write(str(D[pid][k])+'\t')
				fout.write('\n')
			fout.close()
	#fout.close()




Generate_dcpg_TriCpG('chr11',100820988,100822590,'Stat5b')
Generate_dcpg_TriCpG('chr11',100825906,100827199,'Stat5b')

Generate_dcpg_TriCpG('chr11',100861283,100861908,'Stat5a')


Generate_dcpg_TriCpG('chr6',88190741,88191157,'Gata2')
Generate_dcpg_TriCpG('chr6',88190532,88190741,'Gata2')

Generate_dcpg_TriCpG('chr3',30510531,30512987,'Mecom')
Generate_dcpg_TriCpG('chr3',30008761,30009814,'Mecom')
Generate_dcpg_TriCpG('chr3',30007030,30007578,'Mecom')
Generate_dcpg_TriCpG('chr3',30004347,30005320,'Mecom')

Generate_dcpg_TriCpG('chr12',12936548,12937108,"Nmyc")
