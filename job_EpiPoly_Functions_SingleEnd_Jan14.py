import pysam
import os
import bisect
import sys

pid=sys.argv[1]
chrn=sys.argv[2]

######### paramters ################

PAD=10
posfile='/home/wangq/Daniel_TWGBS/mm9_CG/mm9.CG_CH.pos.gz'
MIN_QUAL=20 # minimal base quality

MIN_CPG=3 # minimal number of CpGs per read to be plotted
SampleList=['MPP_1a','MPP_1b','MPP_2a','MPP_2b']

MINcov=30

############## constant defined
METH=1
UNMETH=2
UNKOWN=0
SOFTCLIP_CIGAR=4

########## loop over read in this region ########
d_cg_nt={0:('C','T'),1:('G','A')}

##### functions #################33
def find_gt(a, x):
  'Find leftmost value greater than x'
  i = bisect.bisect_right(a, x)
  if i != len(a):
    return i
  raise ValueError

def merge_d(d1,d2): # merged d2 into d1
  for k1 in d2.keys():
    if not d1.has_key(k1):
      d1[k1]={}
    for k2 in d2[k1].keys():
      if not d1[k1].has_key(k2):
        d1[k1][k2]=d2[k1][k2]
      else:
        d1[k1][k2]+=d2[k1][k2]


def sort_by_mean(ks):
  d={}
  for k in ks:
    n=0
    for kk in k:
      if kk=='0':
	n=n+1.5
      else:
      	n=n+int(kk)
    m=1.0*n/len(k)
    if not d.has_key(m):
      d[m]=[k]
    else:
      d[m].append(k)
  l=[]
  ks=d.keys()
  ks.sort()
  for k in ks:
    for x in d[k]:
      l.append(x)
  return l


def Merge(s1,s2):
  sUNKOWN=str(UNKOWN)
  L1=len(s1)
  if len(s2)!=L1:
    return -1
  s=''
  for i in range(L1):
    if s1[i]==s2[i]:
      s=s+s1[i]
    elif s1[i]==sUNKOWN:
      s=s+s2[i]
    elif s2[i]==sUNKOWN:
      s=s+s1[i]
    else:
      s=s+sUNKOWN
  return s

	

def EpiPoly(bamfile,chrn,start,end,poslist):
  Nhomo=0
  Nhetero=0
  Nmeth=0
  Nunmeth=0
  Lposlist=len(poslist)
  dmate={} # before merge into d_cpg_bin, read name is kept for merge read pairs
  d_cpg_bin={}
  samfile = pysam.Samfile( bamfile, "rb" )
  #result_list=[]
  #retrieve all reads that are mapped to the interval specified
  for alignedread in samfile.fetch( chrn,start,end):
    # skip aligned reads with indel
    # the only possible cigar is one unit with 0 or two unit with 0 and 4
    cigar=alignedread.cigar
    if cigar==None:
      #print "no cigar string"
      continue
    Lcigar=len(cigar)
    if Lcigar>2:
      #print "there is indel in the read"
      continue
    if Lcigar==2:
      if cigar[0][0]!=SOFTCLIP_CIGAR and cigar[1][0]!=SOFTCLIP_CIGAR:
        continue
    if ( alignedread.is_read1 and not alignedread.is_reverse) or ( alignedread.is_read2 and alignedread.is_reverse):
      #CTread=True
      cg_shift=0
    else:
      #CTread=False
      cg_shift=1
    (cg_ref_nt,cg_conv_nt)=d_cg_nt[cg_shift]
    # if the index is further than the maped reads
    if poslist[-1]<alignedread.pos:
     # print "the read is beyond the indexed CpG"  
      continue
    seq=alignedread.seq
    # find the first CpG in the read
    CpG_local_index=find_gt(poslist,alignedread.pos-1)
    # remember the starting local index, as the non-start one will be overwritten
    CpG_local_index_start=CpG_local_index
    # get the acutally genomic corrdinates given the local index
    CpGpos=poslist[CpG_local_index]
    # initiate an empty string to store the digital methylation information, such as '111202'
    read_cpg_bin=''
    loop=True
    # loop over all CpG that fell in the read region
    while loop and CpG_local_index<Lposlist:
      CpGpos=poslist[CpG_local_index]
      if CpGpos>=alignedread.aend:
        loop=False
        continue
      CpGpos=poslist[CpG_local_index]
      qpos=CpGpos-alignedread.pos+alignedread.qstart+cg_shift
      if qpos>=alignedread.qlen:
        break
      # CpG_nt=seq[qpos:qpos+2] # without addding cg_shift
      # CpG_bin is the value showing the methylation status 0 1 or 2 
      CpGqual=ord(alignedread.qual[qpos]) # check for base quality
      if CpGqual > (MIN_QUAL+33):
        CpGnt=seq[qpos]
        if CpGnt==cg_conv_nt: # 
          CpG_bin=UNMETH # non-methylated
          Nunmeth +=1
        elif CpGnt==cg_ref_nt:
          CpG_bin=METH # methylated
          Nmeth +=1
        else:
          CpG_bin=UNKOWN # no information
      else:
        CpG_bin=UNKOWN
      read_cpg_bin+=str(CpG_bin)
      CpG_local_index+=1
    if not dmate.has_key(alignedread.qname):
      dmate[alignedread.qname]=[]
    dmate[alignedread.qname].append((CpG_local_index_start,read_cpg_bin))
  for v in dmate.values():
    Lv=len(v)
    if Lv==1:
      (CpG_local_index_start,read_cpg_bin)=v[0]
    elif Lv==2:
      (CpG_local_index_start,read_cpg_bin)=MergeReadPair(v[0],v[1])
    else:
	print "error: more than two reads in a pair..."
    # if there are less than the number of minimal CpG sites in the read, then skip
    if len(read_cpg_bin)>=MIN_CPG:
      #print (alignedread.qname,CpG_local_index_start,read_cpg_bin)
      if not d_cpg_bin.has_key(read_cpg_bin):
        d_cpg_bin[read_cpg_bin]={}
      if not d_cpg_bin[read_cpg_bin].has_key(CpG_local_index_start):
        d_cpg_bin[read_cpg_bin][CpG_local_index_start]=0
      d_cpg_bin[read_cpg_bin][CpG_local_index_start] += 1
  return d_cpg_bin


def DiverstiyMeth(nlist,methlist):
        nsum=sum(nlist)
        if nsum<MINcov:
                return -1
        meth=sum(methlist)/NCpG
        n2sum=0
        for n in nlist:
                n2sum += n*(n-1)
        N2=nsum*(nsum-1)
        return (1-1.0*n2sum/N2,meth)


def CalMeth(d_cpg_bin):
	nmeth=[0,0,0]
	ntotal=[0,0,0]
	meths=[]
	for pattern in d_cpg_bin.keys():
		if str(UNKOWN) in pattern: # if there is low quality base in it, exclude from calculation
			continue
		for index_start in d_cpg_bin[pattern].keys():
			n=d_cpg_bin[pattern][index_start]
			i=index_start
			for p in pattern:
				p=int(p)
				if p==UNKOWN:
					continue
				if p==METH:
					nmeth[i]+=n
				ntotal[i]+=n
				i+=1
	for i in range(3):
		if ntotal[i]==0:
			return -1
		else:
			meths.append(1.0*nmeth[i]/ntotal[i])
	return meths



NCpG=3
Npop=pow(2,NCpG)

chrn='chr'+str(chrn)
bamfile='/home/wangq/Daniel_TWGBS/result/results_per_pid/'+pid+'_temp/alignment/'+pid+'_merged.rmdup.bam'
ofile='/home/wangq/Daniel_TWGBS/result/EpiPoly/AllRegion/EpiScore_Meth/'+pid+'_'+chrn+'_EpiScore_Meth.csv'
fout=open(ofile,'w')
if 1:
	if 1:
		ifile='/home/wangq/Daniel_TWGBS/result/EpiPoly/AllRegion/'+pid+'_merged.CG.'+chrn+'.EpiInterval.csv'
		f=open(ifile)
		l=f.readline()
		for l in f:
			ls=l[:-1].split('\t')
			pos1=int(float(ls[1]))
                        pos2=int(float(ls[2]))
                        pos3=int(float(ls[3]))
			poslist0=[pos1,pos2,pos3]
			start=pos1-100
        		end=pos3+100
			#methlist=[float(ls[4]),float(ls[5]),float(ls[6])]
                	d_cpg_bin=EpiPoly(bamfile,chrn,start,end,poslist0)
			#print d_cpg_bin
			methlist=CalMeth(d_cpg_bin)
			if methlist==-1:
				continue
			nlist=[]
			for k,x in d_cpg_bin.items():
				if str(UNKOWN) in k: # skip pattern with unkown base
					continue
				nlist.append(x[0])
			result=DiverstiyMeth(nlist,methlist)
			if result!=-1:
				if result[1]<0.1:
					print d_cpg_bin,methlist,result
				fout.write(chrn+'\t'+str(pos1)+'\t'+str(result[0])+'\t'+str(result[1])+'\n')
	



fout.close()
