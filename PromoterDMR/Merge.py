def Add2D3(d,k1,k2,k3,c):
	if not d.has_key(k1):
		d[k1]={}
	if not d[k1].has_key(k2):
		d[k1][k2]={}
	d[k1][k2][k3]=c


def Add2D2(d,k1,k2,c):
        if not d.has_key(k1):
                d[k1]={}
       	d[k1][k2]=c


DIR='/home/wangq/Daniel_TWGBS/result/EpiPoly/PromoterDMR/'
pops=['LT','ST','MPP2','MPP']
ofile=DIR+'All_merged_EpiScore_Meth_PromoterDMR.csv'

d={}
dgene={}

for pop in pops:
	f=open(DIR+pop+'_EpiScore_Meth_PromoterDMR.bed')
	for l  in f:
		ls=l.split("\t")
		chrn=ls[0]
		pos=ls[1]
		cont=ls[2:5]
		ID=ls[5:7]
		Add2D3(d,chrn,pos,pop,cont)	
		Add2D2(dgene,chrn,pos,ID)	
	

fout=open(ofile,'w')
fout.write('chr\tpos\tEnsembleID\tGeneName\t')
for pop in pops:
	fout.write(pop+'_EpiScore\t'+pop+'_MethLevel\t'+pop+'_EpiScore_Normalized\t')

fout.write('\n')

for chrn in d.keys():
	for pos in d[chrn].keys():
		geneinfo=dgene[chrn][pos]
		fout.write(chrn+'\t'+pos+'\t'+geneinfo[0]+'\t'+geneinfo[1][2:-1]+'\t')
		for pop in pops:
			if d[chrn][pos].has_key(pop):
				cont=d[chrn][pos][pop]
				fout.write(cont[0]+'\t'+cont[1]+'\t'+cont[2]+'\t')
			else:
				fout.write('NA\tNA\tNA\t')		
		fout.write('\n')


fout.close()




