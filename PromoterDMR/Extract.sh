# extract promoter DMR
cd /home/wangq/Daniel_TWGBS/result/results_per_pid/DMRannotationOct18
cat merged_All_merged_EndAdd1MethylationCallSummary_MinCov30_RawMethFiltered_merged_Ensemble_annotation.txt | awk -F '\t' '$10>(-500) && $10<2000 { printf "%s\t%s\t%s\t%s\tG_%s\n",$2,$3,$4,$11,$16 }' >/home/wangq/Daniel_TWGBS/result/EpiPoly/PromoterDMR/merged_All_merged_EndAdd1MethylationCallSummary_MinCov30_RawMethFiltered_merged_Ensemble_annotation_PromoterDMR.bed

# overlap Episcores to promotoer DMR
DMRbed=/home/wangq/Daniel_TWGBS/result/EpiPoly/PromoterDMR/merged_All_merged_EndAdd1MethylationCallSummary_MinCov30_RawMethFiltered_merged_Ensemble_annotation_PromoterDMR.bed

for pop in LT ST MPP2 MPP
	do
	popEpifile=/home/wangq/Daniel_TWGBS/result/EpiPoly/AllRegion/3CpG/EpiScore_Meth_PointTwo2Eight_MethLevelByCountPop_HighQualBaseOnly/${pop}EpiScore_Meth.bed
	ofile=/home/wangq/Daniel_TWGBS/result/EpiPoly/PromoterDMR/${pop}_EpiScore_Meth_PromoterDMR.bed
	intersectBed -wo -a $popEpifile -b $DMRbed | awk -F '\t' {'printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\n",$1,$2,$4,$5,$6,$10,$11 }' >$ofile
done


