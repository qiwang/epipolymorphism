#!/bin/bash

#PBS -l walltime=100:00:00
#PBS -l nodes=1:lsdf
#PBS -l mem=1g
#PBS -m a

#python /home/wangq/METH/Daniel_Mm/Script/EpiSubPopulation/job_EpiPoly_Functions_MergeReadPair_Dec2nd.py $pid $chrn
python /home/wangq/METH/Daniel_Mm/Script/EpiSubPopulation/job_EpiPoly_Functions_MergeReadPair_4CpG.py $pid $chrn
#python /home/wangq/METH/Daniel_Mm/Script/EpiSubPopulation/job_EpiPoly_Functions_MergeReadPair_4CpG_withDoc.py $pid $chrn
