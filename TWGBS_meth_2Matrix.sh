OUTDIR='/home/wangq/Daniel_TWGBS/result/EpiPoly'
DIR_OUTPUT_STEM='/home/wangq/Daniel_TWGBS/result'

#filetag='AllDMR_3rep'
filetag='LT_ST_MPP'
cd $OUTDIR

posfile=$OUTDIR/${filetag}_PopulationCount.pos

#posfile=/home/wangq/Daniel_TWGBS/result/EpiPoly/minCpG_3_test/LT_chr10:11344383_11344390_minCpG_3_Epi.pos

outfile=$posfile.meth
#rm $outfile

echo -e "chrn\tstart\tend\tLT\tST\tMPP" >$outfile
while read LINE 
        do
	chrn=`echo $LINE | awk '{ print $1}'`
	startx=`echo $LINE | awk '{ print $2}'`
	endx=`echo $LINE | awk '{ print $3}'`
	TWGBS=''
	for PID in LT ST MPP
        	do	
		analysis_dir=${DIR_OUTPUT_STEM}/results_per_pid/${PID}_temp/MethylationCall
		tfile=$analysis_dir/${PID}_merged.CG.$chrn.call.gz
		TWGBSx=`tabix $tfile $chrn:$startx-$endx | cut -f6,7 | awk '{T1+=$1;T2+=$2} END {printf "%s,%s\t",T1,T2}'`
		TWGBS=$TWGBS$TWGBSx	
	done
	echo -e "$chrn\t$startx\t$endx\t$TWGBS" >>$outfile
done <$posfile

sort -k1,1 -k2,2n $outfile | uniq | bgzip >$outfile.gz
tabix -f -s 1 -b 2 -e 3 $outfile.gz







