from string import *
DMREpiFile='/home/wangq/Daniel_TWGBS/result/EpiPoly/AllRegion/EpiScore_Meth_PointTwo2Eight_MethLevelByCountPop_HighQualBaseOnly/LTEpiScore_Meth_sorted_Annotation2Gene.bed'
dDMR={}
f=open(DMREpiFile)
for l in f:
	ls=l[:-1].split('\t')
	pos=ls[0]+':'+ls[1]
	gname=upper(ls[5])
	dis=ls[3]
	EpiNorm=float(ls[11])
	if not dDMR.has_key(gname):
		dDMR[gname]={}
	if not dDMR[gname].has_key(pos):
		dDMR[gname][pos]={'dis':dis,'EpiScore':[]}
	dDMR[gname][pos]['EpiScore'].append(EpiNorm)


f.close()	

dmap={'red':'gain','blue':'loss'}
GSEAfile='/home/wangq/METH/Daniel_Mm/report/ManuscriptData/GSEA_core/GSEA_core-enriched_genes_HSC_MPP1_RNAseq.csv'
dGSEA={}
f=open(GSEAfile)
l=f.readline()
for l in f:
	ls=l[:-1].split('\t')
	gname=ls[0]
	dGSEA[gname]=dmap[ls[6]]


dDMR2={}
for tag in ['hyper','hypo']:
	filename='/home/wangq/Daniel_TWGBS/result/results_per_pid/DMROct18mm10/merged_LT_vs_ST_merged_EndAdd1MethylationCallSummary_MinCov30_RawMethFiltered_'+tag+'.bed'
	f=open(filename)
	for l in f:
		ls=l[:-1].split('\t')
		pos=ls[0]+':'+str(int(ls[1])+1)
		dDMR2[pos]={'Direction':tag,'Pos':ls[0]+':'+ls[1]+'-'+ls[2]}
	f.close()


Ofile='/home/wangq/Daniel_TWGBS/result/EpiPoly/AllRegion/EpiScore_Meth_PointTwo2Eight_MethLevelByCountPop_HighQualBaseOnly/LTEpiScore_Meth_sorted_Annotation2Gene_summary.csv'
fout=open(Ofile,'w')
fout.write('GeneSymble\tDMR\tDistance2TSS\tGSEATag\tDMRGain(Hyper)Loss(Hypo)\tMinNormEpiScore\tMaxNormEpiScore\tAverageNormEpiScore\n')
for gname in dDMR.keys():
	if dGSEA.has_key(gname):
          	GSEAtag=dGSEA[gname]
       	else:
              	GSEAtag='NA'
	for pos in dDMR[gname].keys():
		dis=dDMR[gname][pos]['dis']
		scores=dDMR[gname][pos]['EpiScore']
		smax=max(scores)
		smin=min(scores)
		smean=1.0*sum(scores)/len(scores)
me/wangq/Dan
		if dDMR2.has_key(pos):
			DMRtag=dDMR2[pos]['Direction']
		else:
			DMRtag=''
		fout.write(gname+'\t'+dDMR2[pos]['Pos']+'\t'+dis+'\t'+GSEAtag+'\t'+DMRtag+'\t'+str(smin)+'\t'+str(smax)+'\t'+str(smean)+'\n')


fout.close()

genelist=['GATA2','MECOM','GFI1','GFI1B','PRDM16','TGFBR3','PBX1','MYCN','FLT3','RARA','FLI1','RPS10','SERPINH1','POU2F2']

fout=open('/home/wangq/METH/Daniel_Mm/report/ManuscriptData/EpiPoly/EpiPoly_Regions.bed','w')
f=open('/home/wangq/Daniel_TWGBS/result/EpiPoly/AllRegion/EpiScore_Meth_PointTwo2Eight_MethLevelByCountPop_HighQualBaseOnly/LTEpiScore_Meth_sorted_Annotation2Gene_summary.csv')

for l in f:
	ls=l[:-1].split('\t')
	gene=ls[0]
	if not gene in genelist:
		continue
	pos=ls[1]
	chrn=pos.split(":")[0]
	se=pos.split(":")[1]
	(start,end)=se.split('-')
	fout.write(chrn+'\t'+start+'\t'+end+'\t'+gene+'\n')


f.close()
fout.close()





