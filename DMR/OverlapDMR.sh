EpiDIR=/home/wangq/Daniel_TWGBS/result/EpiPoly/AllRegion/EpiScore_Meth_PointTwo2Eight_MethLevelByCountPop_HighQualBaseOnly
for pid in LT ST MPP2 MPP
	do
	Epifile=$EpiDIR/${pid}EpiScore_Meth.csv
	for i in `seq 1 9`
		do
		clusterBED=/home/wangq/METH/Daniel_Mm/report/Oct18/bed/mm10/Merged_mm10_cluster${i}outof9_sorted.bed
		ofile=$EpiDIR/OverlapDMR/${pid}EpiScore_Meth_Cluster${i}.csv
		cat $Epifile | awk '{printf "%s\t%s\t%s\t%s\t%s\n",$1,$2,$2+1,$3,$4}' | sortBed -i - | intersectBed -u -a - -b $clusterBED >$ofile
	done
done

for step in LT_vs_MPP2 LT_vs_MPP LT_vs_ST MPP2_vs_MPP ST_vs_MPP2 ST_vs_MPP
	do
	for typ in hyper hypo
		do
		BED=/home/wangq/Daniel_TWGBS/result/results_per_pid/DMROct18mm10/merged_${step}_merged_EndAdd1MethylationCallSummary_MinCov30_RawMethFiltered_${typ}.bed
		for pid in LT ST MPP2 MPP
			do
			Epifile=$EpiDIR/${pid}EpiScore_Meth.csv
			ofile=$EpiDIR/OverlapDMR/${pid}EpiScore_Meth_Step_${step}_${typ}.csv
			cat $Epifile | awk '{printf "%s\t%s\t%s\t%s\t%s\n",$1,$2,$2+1,$3,$4}' | sortBed -i - | intersectBed -u -a - -b $BED >$ofile
		done
	done
done


step=LT_vs_ST
pid=LT
Anno=/home/wangq/Daniel_TWGBS/result/results_per_pid/DMRannotationOct18/merged_LT_vs_ST_merged_EndAdd1MethylationCallSummary_MinCov30_RawMethFiltered_annotation.txt
Epifile=$EpiDIR/${pid}EpiScore_Meth.bed
sortBed -i $Epifile >$EpiDIR/${pid}EpiScore_Meth_sorted.bed

tail -n+2 $Anno | awk -F '\t' '{printf "%s\t%s\t%s\t%s\t%s\t%s\n",$2,$3,$4,$10,$15,$16}' | sortBed -i - | intersectBed  -wo -a - -b $EpiDIR/${pid}EpiScore_Meth_sorted.bed >$EpiDIR/${pid}EpiScore_Meth_sorted_Annotation2Gene.bed

# python convert Epifile to bed file with the EpiScore_Norm

def MaxEpi(meth):
  N=30
  n1=pow((1-meth),3)*N
  n2=pow((1-meth),2)*meth*N
  n3=(1-meth)*pow(meth,2)*N
  n4=pow(meth,3)*N
  return (1-1.0*(n1*(n1-1)+3*n2*(n2-1)+3*n3*(n3-1)+n4*(n4-1))/N/(N-1))
	

# if there is just two population meth and unmeth
# the chance that the two population are different 
# is that one CpG is diffent from the other
def MinEpi(meth):
  return(meth*(1-meth)*2)


for pop in ['ST','MPP','MPP2']:
	f=open('/home/wangq/Daniel_TWGBS/result/EpiPoly/AllRegion/3CpG/EpiScore_Meth_PointTwo2Eight_MethLevelByCountPop_HighQualBaseOnly/'+pop+'EpiScore_Meth.csv')
	fout=open('/home/wangq/Daniel_TWGBS/result/EpiPoly/AllRegion/3CpG/EpiScore_Meth_PointTwo2Eight_MethLevelByCountPop_HighQualBaseOnly/'+pop+'EpiScore_Meth.bed','w')
	for l in f:
		ls=l[:-1].split('\t')
		chrn=ls[0]
		pos=int(ls[1])
		EpiScore=float(ls[2])
		Meth=float(ls[3])
		EpiMax=MaxEpi(Meth)
		EpiMin=MinEpi(Meth)
		EpiScoreNorm=(EpiScore-EpiMin)/(EpiMax-EpiMin)
		fout.write(chrn+'\t'+ls[1]+'\t'+str(pos+1)+'\t'+ls[2]+'\t'+ls[3]+'\t'+str(EpiScoreNorm)+'\n')
	f.close()
	fout.close()



