import gzip
import sys

# diversity calcuated based on all possiblities
def DiverstiyMeth(nlist):
	if len(nlist)!=Npop:
		return -1
	nsum=sum(nlist)
	if nsum<MINCOV:
		return -1
	methsum=0
	n2sum=0
	for i in range(Npop):
		n=nlist[i]
		n2sum += n*(n-1)
		methsum += METHINDEX[i]*n
	N2=nsum*(nsum-1)
	#print n2sum,N2	
	return (1-1.0*n2sum/N2,methsum/nsum)

# Diversity Calculated Eliminating fully methylated and fully protected 
def DiverstiyMethV1(nlist):
        if len(nlist)!=Npop:
                return -1
        methsum=0
        n2sum=0
	nsum=0
        for i in range(Npop):
                n=nlist[i]
                methsum += METHINDEX[i]*n
                if i==0 or i==Npop-1:
			continue
                n2sum += n*(n-1)
		nsum += n
	if nsum<MINCOV:
                return -1
        N2=nsum*(nsum-1)
        #print n2sum,N2 
        return (1-1.0*n2sum/N2,methsum/sum(nlist))

# return the frequency of the fully methylated and full protected subpopulation, as well as the methylation level of the region
def FullMethOrProtect(nlist):
	if len(nlist)!=Npop:
                return -1
        nsum=sum(nlist)
        if nsum<MINCOV:
                return -1
	methsum=0
	for i in range(Npop):
		n=nlist[i]
                methsum += METHINDEX[i]*n
	return (1.0*nlist[0]/nsum,1.0*nlist[Npop-1]/nsum,methsum/nsum)


METH='1'
UNMETH='2'
UNKOWN='0'

METHINDEX=[]
for x in [1,2]:
        for y in [1,2]:
                for z in [1,2]:
                        METHINDEX.append((3*2-x-y-z)/3.0)


NCpG=3
NCpGextend=NCpG-1
Npop=pow(2,NCpG)

MINCOV=10 # minimal coverage in the region
pid=sys.argv[1] #'MPP'
dmrtag=sys.argv[2] # can be empty '.l_s_cg_dmr_t_sorted'
# option 1
#fout=gzip.open('/home/wangq/Daniel_TWGBS/result/results_per_pid/LT_temp/alignment/'+pid+'_epi'+dmrtag+'_all_min'+str(MINCOV)+'.gz','wb')
#NSKIP=0
# option 1.1
fout=gzip.open('/home/wangq/Daniel_TWGBS/result/results_per_pid/LT_temp/alignment/'+pid+'_epi_middle'+dmrtag+'_all_min'+str(MINCOV)+'.gz','wb')
# option 2
#fout=gzip.open('/home/wangq/Daniel_TWGBS/result/results_per_pid/LT_temp/alignment/'+pid+'_fullmethprotect'+dmrtag+'_all_min'+str(MINCOV)+'.gz','wb')
#NSKIP=0
for chrn in range(1,20):
	f=gzip.open('/home/wangq/Daniel_TWGBS/result/results_per_pid/'+pid+'_temp/alignment/'+pid+'_merged.epi'+dmrtag+'.chr'+str(chrn)+'.gz','rb')
	for l in f:
		ls=l[:-1].split('\t')
		pos=ls[0]
		nxlist=ls[1].split(',')[:Npop]
		nlist=[]
		for x in nxlist:
			nlist.append(int(x))
		# option 1
		# result=DiverstiyMeth(nlist)
		# option 1.1
                result=DiverstiyMethV1(nlist)
		if result==-1:
			continue
		(diversity,meanmeth)=result
		fout.write('chr'+str(chrn)+'\t'+pos+'\t'+ls[1]+'\t'+str(diversity)+'\t'+str(meanmeth)+'\n')
		# option 2
		"""
		result=FullMethOrProtect(nlist)
		if result==-1:
			continue
		fout.write('chr'+str(chrn)+'\t'+pos+'\t'+ls[1]+'\t'+str(result[0])+'\t'+str(result[1])+'\t'+str(result[2])+'\n')
		"""
	f.close()


fout.close()



